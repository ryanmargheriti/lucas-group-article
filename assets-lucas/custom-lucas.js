$(function() {

  var getViewportHeight = function() {
    return Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
  }
  var statsDuration = function() {
    return getViewportHeight() * 2.2;
  }
  var duration = getViewportHeight();

  // Init ScrollMagic Controller
  var scrollMagicController = new ScrollMagic({
    globalSceneOptions: {
      triggerHook: 'onLeave'
    }
  });

  // Clock object
  var clock = {
    animate: {},
    face: '#clockFace',
    minutes: '#clockMinutes',
    hours: '#clockHours'
  }


  function calcHourRotation(h) {
    return 360/12*h;
  }
  function calcMinsRotation(h, m) {
    return 360*(h)+360*(m/60);
  }

  function setClockTimeTrigger(that) {
    var time = that.attr('data-time-trigger');
    var hours = time.split(':')[0];
    var minutes = time.split(':')[1];

    // Scroll the minutes
    clock.animate.minutes = new ScrollScene({
      triggerElement: that,
      offset: -250,
      duration: 300
    })
    .setTween(TweenMax.to(clock.minutes, 0.5, {
      rotation: calcMinsRotation(hours, minutes)
    }))
    .addTo(scrollMagicController);

    // Scroll the hours
    clock.animate.hours = new ScrollScene({
      triggerElement: that,
      offset: -250,
      duration: 100
    })
    .setTween(TweenMax.to(clockHours, 0.5, {
      rotation: calcHourRotation(hours)
    }))
    .addTo(scrollMagicController);
  }

  function activateFootnote(id) {
    var that = $('[data-footnote='+id+']');
    that.addClass('active');
    $('body').one('mouseup', function() {
      that.removeClass('active');
    });
  }

  $('.jumbotron-img').one("load", function() {
    addScrollMagicToElement($(this));
    $(this).parents('.jumbotron').addClass('active');
  }).each(function() {
    if(this.complete) $(this).load();
  });


  // Animate the pullquotes when they come into view
  // --------------------------------------------------
  $('.pullquote').each(function() {
    var that = $(this);
    var id = that.data('expandcontent-trigger');
    var el = $('.pullquote[data-expandcontent-trigger='+id+']');

    var scene = new ScrollScene({
      triggerElement: el,
      offset: -500,
      duration: 100,
      duration: 1500
    })
    .setClassToggle(el, "active")
    .addTo(scrollMagicController);
  });

  // Animate the images
  // --------------------------------------------------
  function addScrollMagicToElement(that) {
    var scene = new ScrollScene({
      triggerElement: that,
      offset: -getViewportHeight(),
      duration: 100
    })
    .on('enter', function() {
      that.addClass('active');
      that.removeClass('inactive');
    })
    .addTo(scrollMagicController);
    that.addClass('inactive');
  }

  $('.activate-on-scroll-enter').each(function(i) {
    var that = $('.activate-on-scroll-enter').eq(i);
    addScrollMagicToElement(that);
  });

  // Fade out time labels
  // --------------------------------------------------
  // $('.time-label').each(function() {
  //   var that = $(this);
  //   var time = that.data('time');
  //   var scene = new ScrollScene({
  //     triggerElement: $('.time-label[data-time="'+time+'"]'),
  //     offset: -250
  //   })
  //   .setClassToggle($('.time-label[data-time="'+time+'"]'), "inactive")
  //   .addTo(scrollMagicController);
  // });

  $('[data-time-trigger]').each(function() {
    var that = $(this);
    var time = that.data('time-trigger');
    var scene = new ScrollScene({
      triggerElement: $('[data-time-trigger="'+time+'"]'),
      offset: -500,
      duration: 550
    })
    .setClassToggle($('.time-label[data-time="'+time+'"]'), "active")
    .addTo(scrollMagicController);
  });



  // Page takeovers
  // --------------------------------------------------


  // for (var i = 0; i <= 3; i++) {
  //   new ScrollScene({triggerElement: "#statsSection", duration: duration, offset: duration*i})
  //         .setClassToggle("#statsSection", "active-"+i+1)
  //         .addTo(scrollMagicController);
  // };



  if(!Modernizr.touchevents) {
    new ScrollScene({triggerElement: "#statsSection", duration: statsDuration})
            .setPin('#statsSection')
            .addTo(scrollMagicController);
    new ScrollScene({triggerElement: "#statsSection", duration: duration})
            .setClassToggle("#statsSection", "active-1")
            .addTo(scrollMagicController);
    new ScrollScene({triggerElement: "#statsSection", duration: getViewportHeight, offset: duration/2})
            .setClassToggle("#statsSection", "active-2")
            .addTo(scrollMagicController);
    new ScrollScene({triggerElement: "#statsSection", duration: getViewportHeight, offset: duration})
            .setClassToggle("#statsSection", "active-3")
            .addTo(scrollMagicController);
    new ScrollScene({triggerElement: "#statsSection", duration: getViewportHeight, offset: duration*1.5})
            .setClassToggle("#statsSection", "active-4")
            .addTo(scrollMagicController);
    new ScrollScene({triggerElement: "#statsSection", duration: getViewportHeight, offset: duration*2})
            .setClassToggle("#statsSection", "active-5")
            .addTo(scrollMagicController);
  }




  // Expand Content
  // --------------------------------------------------
  (function() {
    // Todo
    // ----
    // - Remove the closing even handlers
    // - Let the user use the ESC key to close the expanded content
    // - Add an option in to activate the shade so this can be used for footnotes etc

    var body = document.querySelector('body');
    var triggers = document.querySelectorAll('[data-expandContent-trigger]');

    var createBodyOverlay = function() {
      var el = document.createElement('div');
      body.appendChild(el).classList.add('shade');
    };

    var getDetail = function(id) {
      return document.querySelector('[data-expandContent-detail="'+id+'"]')
    };

    var openDetail = function(id) {
      var detail = getDetail(id);
      if(!detail) { return false; }
      detail.classList.add('is-active');
      body.classList.add('has-detail');
      attachCloseHandlers(id);
    };

    var closeDetail = function(id) {
      var detail = getDetail(id);
      if(!detail) { return false; }
      detail.classList.remove('is-active');
      body.classList.remove('has-detail');
    };

    var attachClickHandlers = function(that, id) {
      that.addEventListener('click', function() {
        openDetail(id);
        attachCloseHandlers(id);
      });
    }

    var handleTriggers = function() {
      for (var i = triggers.length - 1; i >= 0; i--) {
        var id = triggers[i].getAttribute('data-expandContent-trigger');
        attachClickHandlers(triggers[i], id);
      };
    };

    var attachCloseHandlers = function(id) {
      var activeDetail = getDetail(id);
      var click  = function(event) {
        if(event.target.dataset['closeContent'] != undefined) {closeDetail(id);}
        if (event.target == activeDetail || event.target.parentNode == activeDetail) {return false;}
        closeDetail(id);
      }
      document.addEventListener('mouseup', click);
    };

    var init = function() {
      handleTriggers();
      createBodyOverlay();
    }

    init();

  })();


  // Footnotes
  // --------------------------------------------------
  $('[data-footnote-trigger]').on('click', function() {
    var id = $(this).attr('data-footnote-trigger');
    activateFootnote(id)
  })


  // Image Gallery
  // --------------------------------------------------
  $('.gallery-cell').on('mousedown', function() {
    $('.gallery').flickity().flickity('next');
  });


  // All clock rotation animations
  // --------------------------------------------------
  $('[data-time-trigger]').each(function() {
    var that = $(this);
    setClockTimeTrigger(that);
    // addTimeLabel(that);
  });


  // Dock the clock when user hits timeline section
  // --------------------------------------------------
  clock.dock = new ScrollScene({
    triggerElement: '#clockTrigger',
    offset: -30
  })
  .setPin(clock.face, {
    pushFollowers: false
  })
  .addTo(scrollMagicController);


  // Activate the clock when user hits timeline section
  // --------------------------------------------------
    clock.activate = new ScrollScene({
      triggerElement: '#triggerTimeline',
      // offset: '-86px' // height of clock
    })
    .setClassToggle(clock.face, "active")
    .addTo(scrollMagicController);

  $(window).on('resize', function() {
    clock.dock.destroy(true);

    clock.dock = new ScrollScene({
      triggerElement: '#clockTrigger',
      offset: -30
    })
    .setPin(clock.face, {
      pushFollowers: false
    })
    .addTo(scrollMagicController);
  });

});
